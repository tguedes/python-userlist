# Python Angular User List

Hosted at http://python-uselist-tguedes.s3-website-us-east-1.amazonaws.com

## Run Application Locally

## With Docker

1. In the project root, run `docker-compose up`

### Without Docker

#### Server
**python3.7 is required**

1. Navigate to Server directory
2. Run `pip install -r requirements.txt`
3. Run `flask run --host=127.0.0.1`

#### Client
**node and npm is required**

1. Navigate to Client directory
2. Run `npm install`
3. Run `npm install -g @angular/cli@7.3.9`
4. Run `ng serve`