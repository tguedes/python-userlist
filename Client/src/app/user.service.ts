import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {tap} from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {User} from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private usersUrl = 'http://localhost:5000/users';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl);
  }

  getUser(id: string): Observable<User> {
    const url = `${this.usersUrl}/${id}`;
    return this.http.get<User>(url);
  }

  searchUsers(searchText: string): Observable<User[]> {
    const url = `${this.usersUrl}?q=${searchText}`;
    return this.http.get<User[]>(url);
  }
}
