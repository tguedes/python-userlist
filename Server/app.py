from flask import Flask, jsonify, request, make_response
import uuid
import json

app = Flask(__name__)

class User():
    def __init__(self, username, first_name, last_name, status):
        self.id = str(uuid.uuid1())
        self.username = username
        self.firstName = first_name
        self.lastName = last_name
        self.status = status

    def toJSON(self):
        return self.__dict__

users = [
    User("tguedes", "Thiago", "Guedes", "Active"), 
    User("fred", "Fred", "Durst", "Inactive"), 
    User("123fred123", "Fred", "Red", "Deleted")
]

@app.route("/users")
def users_api():
    response = users
    query_string = request.args.get('q')
    if query_string:
        response = list(filter(lambda x: query_string in x.username, users))

    response = list(map(lambda x: x.toJSON(), response))
    return jsonify(response)

@app.route("/users/<string:user_id>")
def user_api(user_id):
    user_list = list(filter(lambda x: user_id in x.id, users))

    if len(user_list) == 0:
        return make_response("Not Found", 404)

    if len(user_list) > 1:
        return make_response("id not unique", 500)

    user = user_list[0]
    
    return jsonify(user.toJSON())
